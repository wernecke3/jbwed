import React from "react";
import Link from "gatsby-link";
import JBP4K from "../images/JBP4K.jpg";

const IndexPage = () => (
  <div className="text-center">

    <img src={JBP4K} className="block mx-auto " />
    <h1 className="bg-yellow inline-block my-8 p-3">
      John and Brandi are getting married!!
    </h1>
    <h2 className="text-left pb-3">
      They're doing what??
      </h2>
      <p className="text-left text-lg font-serif leading-normal"> 
After 6 years of dating, were you starting to give up on us? As one close family member described it, “You’re like Goldie Hawn and Kurt Russell.” We had to Google that reference but once we did, it seemed apropos. 
<br/><br/>
But recently, both of us knew we wanted to take things to the next level and honor the hard work we’ve put into our relationship. We’ve worked on communication, on compromise, on picking our battles, and on loving each other no matter what. We’ve raised two beautiful tabby cats, Hank and Birdie. We’ve lived in three apartments in two cities and learned how to make life-stuff happen together (hello, finances). We’re best friends, we’re in love, we’re always working to improve ourselves, and we can already see our future together. It’s time to commit to that future.
<br/><br/>
Overall, marriage doesn’t resonate with either one of us. From modern-day wedding traditions not feeling like “us”, to the (let’s face it) sexist origins of a woman being wed to a man, to the fear of divorce. But we decided it’s still the current best way to honor each other and the life we want to continue to build as John and Brandi. 
<br/><br/>
We’re stripping this wedding/marriage thing down to only the stuff that’s important to us. Only the things that make us excited. Only the things that don’t feel forced or uncomfortable. Celebrating with our friends and family is at the top of the list. 
      </p>
  </div>
);

export default IndexPage;
